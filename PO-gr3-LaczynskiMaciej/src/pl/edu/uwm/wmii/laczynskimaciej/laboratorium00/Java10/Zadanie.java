package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java10;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.Stack;

public class Zadanie {

    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Robert Kubica");
        pracownicy.add("Tadusz Sznuk");
        pracownicy.add("Adam Małysz");
        pracownicy.add("Soulja Boy");
        pracownicy.add("Piotr Tokarski");
        pracownicy.add("Paweł, po prostu Paweł");
        System.out.println(pracownicy);
        redukuj(pracownicy, 3);
        System.out.println();
        System.out.println(pracownicy);
        odwroc(pracownicy);
        System.out.println();
        System.out.println(pracownicy);
        String x = "jaja Mam. jaja Zwykłe. jajka nie a jaja smocze to są to Nie,. te mi pokazywać jeszcze zamiar masz jeszcze razy ile. cholera O. wytrzasnąłeś je skąd diaska do jaja, Smocze.";
        System.out.println();
        System.out.println("Original String: " + x);
        System.out.println("Modified String: " + odwrocZdanie(x));
        LinkedList<Integer> digits = new LinkedList<>();
        digits.add(1);
        digits.add(2);
        digits.add(3);
        System.out.println(digits);
        Zadanie.liczby(2137);
        System.out.println();
        Zadanie.Eratostenes(420);
    }

    public static <T> void redukuj(LinkedList<T> workers, int n) {
        for (int i = n - 1; i < workers.size(); i += n - 1) {
            workers.remove(i);
        }
    }

    public static <T> void odwroc(LinkedList<T> ulist){
        LinkedList<T> holder = new LinkedList<>(ulist);
        for(int i =ulist.size()-1, j = 0; i >= 0; i--, j++){
            ulist.set(j, holder.get(i));
        }
    }

    public static String odwrocZdanie(String zdanie){
        String[] words = zdanie.split(" ");
        StringBuilder garbled = new StringBuilder();
        Stack<String> odwrocZdanie = new Stack<>();
        for(String word : words){
            odwrocZdanie.push(word);
            if(word.endsWith(".")){
                while((!odwrocZdanie.empty())){
                    StringBuilder reversedphrase = new StringBuilder();
                    reversedphrase.append(odwrocZdanie.pop());
                    if(odwrocZdanie.empty()){
                        reversedphrase.setCharAt(0, Character.toLowerCase(reversedphrase.charAt(0)));
                        garbled.append(reversedphrase);
                        garbled.append(". ");
                    }
                    else if(reversedphrase.toString().equals(word)){
                        reversedphrase.setCharAt(0, Character.toUpperCase(reversedphrase.charAt(0)));
                        garbled.append(reversedphrase, 0, reversedphrase.length()-1);
                        garbled.append(" ");
                    }
                    else{
                        garbled.append(reversedphrase);
                        garbled.append(" ");
                    }
                }
            }
        }
        return garbled.toString();
    }

    public static void liczby(int num){
        Stack<Integer> indiv = new Stack<>();
        while(num!=0){
            indiv.push(num%10);
            num/=10;
        }
        while (!indiv.empty()){
            System.out.print(indiv.pop()+" ");
        }
    }
    public static void Eratostenes(int n){
        BitSet b = new BitSet(n + 1);
        for (int j = 2; j <= n; ++j) {
            b.set(j);
        }
        int j = 2;
        while (j * j <= n) {
            if (b.get(j)) {
                int k = 2 * j;
                while (k <= n) {
                    b.clear(k);
                    k += j;
                }
            }
            ++j;
        }
        int[] prime= b.stream().toArray();
        for(int x: prime){
            System.out.print(x+" ");
        }
        System.out.println();
    }

}