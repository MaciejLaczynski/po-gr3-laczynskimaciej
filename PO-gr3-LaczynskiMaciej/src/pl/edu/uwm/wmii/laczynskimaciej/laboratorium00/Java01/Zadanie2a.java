package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2a {

    public static void main(String[] args){

        int[] ciag = {1,2,3,4,5,6,7,8,9,10};
        int zsum = 0;

        for(int i =0; i< ciag.length;i++){
            if (Math.abs(ciag[i]) % 2 != 0) {
                zsum += 1;
                System.out.println("Liczba " + ciag[i] + " jest niepodzielna");
            }
        }
        System.out.println("Mamy " + zsum + " liczb nieparzystych.");
    }

}
