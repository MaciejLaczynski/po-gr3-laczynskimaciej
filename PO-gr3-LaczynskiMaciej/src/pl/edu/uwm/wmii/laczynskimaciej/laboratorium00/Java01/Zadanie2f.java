package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2f {

    public static void main(String[] args){

        int[] ciag = {1,2,3,4,5,6,7,8,10,20,30,60,69,420,911,2137};

        int zsum = 0;
        for(int i =1; i< ciag.length; i++){

            if(i % 2 != 0 & ciag[i]%2 == 0) {
                System.out.println("Liczba " + ciag[i] + " spełnia warunki (Numer " + i +")");
                zsum++;
            }

        }
        System.out.println("Mamy " + zsum + " liczb spełniających podane warunki");
    }

}