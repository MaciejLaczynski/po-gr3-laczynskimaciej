package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2h {
    public static void main(String[] args) {
        int[] ciag = {-1,-2,3,4,-5,-10,20,30,-40,60,-69,420,-2137};

        int zsum = 0;

        for (int i = 0; i < ciag.length; i++){

            if(ciag[i] < 0){
                if((ciag[i]*-1) < Math.pow(i,2)) {
                    System.out.println("Liczba " + ciag[i] + " spełnia wymagania");
                }
                else{
                    if(ciag[i] < Math.pow(i,2))
                        System.out.println("Liczba " + ciag[i] + " spełnia wymagania");
                }
                zsum++;
            }
        }
        System.out.println("Mamy " + zsum + " liczb spełniających podane warunki");
    }

}
