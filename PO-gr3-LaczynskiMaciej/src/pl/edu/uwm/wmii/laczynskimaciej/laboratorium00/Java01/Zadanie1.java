package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;
import java.lang.Math;

public class Zadanie1 {
    public static void main(String[] args) {

        int[] ciag = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        double[] bezwzg = {1,2,-3,4,-5,-6,9,-21,37};
        int suma = 0;
        for (int j : ciag) {
            suma = suma + j;
        }

        int iloczyn = 1;
        for (int j : ciag) {
            iloczyn = iloczyn * j;
        }

        double bezwzgledna = 0;
        for (double j : bezwzg) {
            if (j > 0)
                bezwzgledna = bezwzgledna + j;
            else
                bezwzgledna = bezwzgledna + (j * -1);
        }

        double pierwiastek = 0;
        for (double j : bezwzg) {
            if (j > 0)
                pierwiastek = pierwiastek + Math.sqrt(j);
            else
                pierwiastek = pierwiastek + Math.sqrt((j * -1));
        }

        double ilobezwzg = 1;
        for (double j : bezwzg) {
            if (j > 0)
                ilobezwzg = ilobezwzg + j;
            else
                ilobezwzg = ilobezwzg + (j * -1);
        }

        int dokwadratu = 0;
        for (int j : ciag) {
            dokwadratu += Math.pow(j, 2);
        }

        System.out.println("Suma dodawania: " + suma);
        System.out.println("Iloczyn Mnożenia: " + iloczyn);
        System.out.println("Suma Wartości bezwzględnej: " + bezwzgledna);
        System.out.println("Suma kwadratowa: " + pierwiastek);
        System.out.println("Ciąg iloczynu wartości bezwzględnej: " + ilobezwzg);
        System.out.println("Ciąg iloczynu wartości kwadratowej: " + dokwadratu);
        System.out.println("Suma i iloczyn Ciągu: " + suma + " , " +iloczyn);

    }
}