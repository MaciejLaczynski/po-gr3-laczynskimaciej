package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2 {

    public static void main(String[] args) {

        int [] tab = new int [] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int n = 9;

        for(int i = 0; i < n; i++){
            int j, last;
            last = tab[tab.length-1];

            for(j = tab.length-1; j > 0; j--){
                tab[j] = tab[j-1];
            }
            tab[0] = last;
        }

        System.out.println();

        System.out.println("Ciąg n:");
        for (int j : tab) {
            System.out.print(j + " ");
        }
    }
}