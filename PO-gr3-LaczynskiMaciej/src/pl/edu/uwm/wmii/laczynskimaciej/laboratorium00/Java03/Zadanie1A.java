package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java03;

public class Zadanie1A {

    public static void main(String[] args){
        System.out.println(counter("XDDDDDDDDDDDDDDDDDDDDDD"));
    }

    public static String counter(String str){
        int counter = 0;

        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == 'D')
                counter++;
        }
        return "Litera D występuje w " + str + " " + counter + " razy.";
    }
}