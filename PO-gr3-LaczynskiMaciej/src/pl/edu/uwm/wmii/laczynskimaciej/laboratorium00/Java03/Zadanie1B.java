package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java03;

public class Zadanie1B {

    public static void main(String[] args){
        System.out.println(customcounter("Cheemsburbger", "e"));
    }

    public static String customcounter(String str, String subStr){
        int counter = 0;

        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == subStr.charAt(0))
                counter++;
        }
        return "W słowie " + str + " litera " + subStr + " pojawia się " + counter + " razy.";
    }
}
