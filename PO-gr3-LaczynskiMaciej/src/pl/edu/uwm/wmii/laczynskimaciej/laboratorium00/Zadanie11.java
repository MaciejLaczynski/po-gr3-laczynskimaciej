package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00;

public class Zadanie11 {

    public static void main(String[] args) {

        String name = """
                 Pamięć nareszcie ma, czego szukała.
                                
                        Znalazła mi się matka, ujrzał mi się ojciec.
                        Wyśniłam dla nich stół, dwa krzesła. Siedli.
                        Byli mi znowu swoi i znowu mi żyli.
                        Dwoma lampami twarzy o szarej godzinie
                        błyśli jak Rembrandtowi.
                                
                        Teraz dopiero mogę opowiedzieć,
                        w ilu snach się tułali, w ilu zbiegowiskach
                        spod kół ich wyciągałam,
                        w ilu agoniach przez ile mi lecieli rąk.
                        Odcięci – odrastali krzywo.
                        Niedorzeczność zmuszała ich do maskarady.
                        Cóż stąd, że to mogło ich poza mną boleć,
                        jeśli bolało ich we mnie.
                        Śniona gawiedź słyszała, jak wołałam mamo
                        do czegoś, co skakało piszcząc na gałęzi.
                        I był śmiech, że mam ojca z kokardą na głowie.
                        Budziłam się ze wstydem.
                                
                        No i nareszcie.
                        Pewnej zwykłej nocy,
                        z pospolitego piątku na sobotę,
                        tacy mi nagle przyszli, jakich chciałam.
                        Śnili się, ale jakby ze snów wyzwoleni,
                        posłuszni tylko sobie i niczemu już.
                        W głębi obrazu zgasły wszystkie możliwości,
                        przypadkom brakło koniecznego kształtu.
                        Tylko oni jaśnieli piękni, bo podobni.
                        Zdawali mi się długo, długo i szczęśliwie.
                                
                                
                        Zbudziłam się. Otwarłam oczy.
                        Dotknęłam świata jak rzeźbionej rany.
                """;
        System.out.println(name);
    }
}