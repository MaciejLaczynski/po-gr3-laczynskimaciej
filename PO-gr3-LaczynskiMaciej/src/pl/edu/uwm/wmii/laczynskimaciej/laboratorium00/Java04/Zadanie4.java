package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java04;
import java.util.ArrayList;

public class Zadanie4 {

    public static void main(String[] args){

        ArrayList<Integer> arr1 = new ArrayList<>(9);
        arr1.add(1);
        arr1.add(4);
        arr1.add(9);
        arr1.add(16);
        arr1.add(9);
        arr1.add(7);
        arr1.add(4);
        arr1.add(9);
        arr1.add(11);

        System.out.println(arr1);
        System.out.println(mergeReverse(arr1));

    }

    public static ArrayList<Integer> mergeReverse(ArrayList<Integer> a){
        ArrayList<Integer> arrRev = new ArrayList<>();
        int ca = a.size()-1;
        for (int i = 0; i < a.size(); i++){
            arrRev.add(a.get(ca--));
        }
        return arrRev;
    }

}