package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java03;

public class Zadanie1H {

    public static void main(String[] args) {

        System.out.println(nice("21,37", "-", 0));
    }

    public static StringBuilder nice(String str, String select, int sep) {
        StringBuilder s = new StringBuilder(str);
        int id = (str.length() - sep);

        if (sep < 1)
            return s;

        while (id > 0)
        {
            s.insert(id, select);
            id = id - sep;
        }
        return s;
    }
}
