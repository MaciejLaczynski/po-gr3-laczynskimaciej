package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java09;

public class PairUtil<T>{
    public static <T> Pair<T> swap(Pair<T> test){
        Pair<T> prev_pair = new Pair<>(test.getJeden(), test.getDwa());
        test.swap();
        return prev_pair;
    }
}

