package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie2 {

    public static void main(String[] args){
        String a = "C:/Users/PC/Desktop/FolderKurwa/ObiektoweKurwa/IntelliJRepository/PO-gr3-LaczynskiMaciej/src/pl/edu/uwm/wmii/laczynskimaciej/laboratorium00/Java03/XD.txt";
        System.out.println(result("E",a));
    }
    public static int result(String str, String lf){
        int counter = 0;
        try{ File obj = new File(lf);
            Scanner reader = new Scanner(obj);

            while(reader.hasNextLine()){
                String data = reader.nextLine();

                for(int i =0; i < data.length(); i++){

                    if (data.charAt(i) == str.charAt(0))
                        counter ++;
                }
            }
            reader.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("Wystąpił błąd, ścieżka jest nieprawidłowa");
            e.printStackTrace();
            return 0;
        }
        return counter;
    }
}

