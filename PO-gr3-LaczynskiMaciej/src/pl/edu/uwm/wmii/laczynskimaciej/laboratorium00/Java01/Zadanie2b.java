package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2b {

    public static void main(String[] args){

        int[] ciag = {1,2,3,4,5,6,7,8,9,10,12,13,14,15,16,17,18,19,20};
        int zsum = 0;
        for (int j : ciag) {
            int dziel3 = Math.abs(j) % 3;
            int dziel5 = Math.abs(j) % 5;

            if (dziel3 == 0 & dziel5 > 0) {
                zsum += 1;
                System.out.println("Liczba " + j + " jest podzielna przez 3 ale nie jest podzielna przez 5.");
            }
        }
        System.out.println("Mamy " + zsum + " liczb podzielnych przez 3 ale niepodzielnych przez 5");
    }
}
