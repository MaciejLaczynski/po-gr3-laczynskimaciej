package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java04;
import java.util.ArrayList;

public class Zadanie5 {

    public static void main(String[] args){
        ArrayList<Integer> arr1 = new ArrayList<>(9);
        arr1.add(1);
        arr1.add(4);
        arr1.add(9);
        arr1.add(16);
        arr1.add(9);
        arr1.add(7);
        arr1.add(4);
        arr1.add(9);
        arr1.add(11);

        System.out.println(arr1);
        reverse(arr1);
    }

    public static void reverse(ArrayList<Integer> a){
        int size = a.size();
        for (int i = 0; i < size / 2; i++) {
            Integer rev = a.get(i);
            a.set(i, a.get(size - i - 1));
            a.set(size - i - 1, rev);
        }
        System.out.println(a);
    }

}


