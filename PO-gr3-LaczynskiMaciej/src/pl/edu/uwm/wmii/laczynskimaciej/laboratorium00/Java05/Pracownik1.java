package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java05;
import java.util.Date;
import java.util.GregorianCalendar;

public class Pracownik1 {
    class Pracownik implements Cloneable {
        private String nazwisko;
        private double pobory;
        private Date dataZatrudnienia;

        public Pracownik(String var1, double var2) {
            this.nazwisko = var1;
            this.pobory = var2;
        }

        public Object clone() throws CloneNotSupportedException {
            Object var1 = super.clone();
            ((Pracownik)var1).dataZatrudnienia = (Date)this.dataZatrudnienia.clone();
            return var1;
        }

        public void setDataZatrudnienia(int var1, int var2, int var3) {
            this.dataZatrudnienia = (new GregorianCalendar(var1, var2 - 1, var3)).getTime();
        }

        public void zwiekszPobory(double var1) {
            double var3 = this.pobory * var1 / 100.0D;
            this.pobory += var3;
        }

        public String toString() {
            return "Pracownik [nazwisko = " + this.nazwisko + ", pobory = " + this.pobory + ", dataZatrudnienia = " + this.dataZatrudnienia + "]";
        }
    }
}
