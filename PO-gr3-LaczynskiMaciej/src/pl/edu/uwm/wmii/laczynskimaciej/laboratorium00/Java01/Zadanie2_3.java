package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2_3 {
    public static void main(String[] args) {
        int[] ciag = {0,-1,-2,3,-4,0,5,6,-7,8,-9,10,11,0,-12,-13,-14,0,-15,0};
        int dodatnie = 0;
        int ujemne = 0;
        int zero = 0;

        for (int j : ciag) {
            if (j == 0)
                zero++;
            if (j > 0)
                dodatnie++;
            if (j < 0)
                ujemne++;
        }
        System.out.println("Jest " + dodatnie + " liczb dodatnich.");
        System.out.println("Jest " + ujemne + " liczb ujemnych.");
        System.out.println("Jest " + zero + " zer.");
    }
}
