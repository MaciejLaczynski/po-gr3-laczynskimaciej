package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Test2;
import java.util.Collections;
import java.util.ArrayList;
import java.util.HashMap;
import static pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Test2.Kandydat.RecruitmentMap;
import static pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Test2.Kandydat.Qualified;


public class Main {

    public static void main(String[] args) {
        ArrayList<Kandydat> grupa = new ArrayList<>();
        grupa.add(new Kandydat("Robert Kubica", 22, "magister", 1));
        grupa.add(new Kandydat("Anthony Hawk", 28, "licencjat", 2));
        grupa.add(new Kandydat("Adam Malysz", 22, "doktorat", 5));
        grupa.add(new Kandydat("Pawel Jumper", 24, "licencjat", 4));
        grupa.add(new Kandydat("Warol Kojtyla", 25, "magister", 7));
        grupa.add(new Kandydat("Mariusz Krukowski", 25, "magister", 1));

        System.out.print("\nPrzed Sortem:\n");
        for (Kandydat e : grupa) {
            System.out.printf("%s %d, %s, %d\n", e.getNazwa(), e.getWiek(), e.getWyksztalcenie(), e.getDoswiadczenie());
        }

        Collections.sort(grupa);

        System.out.print("\nPo Sorcie:\n");
        for (Kandydat e : grupa) {
            System.out.printf("%s %d, %s, %d\n", e.getNazwa(), e.getWiek(), e.getWyksztalcenie(), e.getDoswiadczenie());
        }

        Rekrutacja.setDoswiadczenie();

        System.out.print("\nKto się zakwalifikował:\n");
        for (Kandydat e : grupa) {
            System.out.println(e.getNazwa() + " " + e.getDoswiadczenie() + " : " + Qualified(e));
        }

        System.out.print("\nZakwalifikowani Kandydaci:\n");
        HashMap<Integer, String> map = RecruitmentMap(grupa);
        for (Integer e : map.keySet()) {
            System.out.println(e + " " + map.get(e));
        }

    }


}