package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java03;

import java.util.Arrays;

public class Zadanie1E {

    public static void main(String[] args){
        System.out.println(Arrays.toString(where("In the first age, in the first battle, when the shadows first lengthened, one stood. Burned by the embers of Armageddon, his soul blistered by the fires of Hell and tainted beyond ascension, he chose the path of perpetual torment.\n" +
                "\n" +
                "In his ravenous hatred he found no peace, and with boiling blood he scoured the Umbral Plains seeking vengeance against the dark lords who had wronged him.\n" +
                "\n" +
                "He wore the crown of the Night Sentinels, and those that tasted the bite of his sword named him... the Doom Slayer.", "e")));
    }

    public static int[] where(String str, String subStr) {
        int pos = 0;
        int len = 0;

        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == subStr.charAt(0))
                len++;
        }

        int[] locator = new int[len];

        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == subStr.charAt(0)) {
                locator[pos] = i;
                pos++;
            }
        }
        return locator;
    }
}
