package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Test;
import java.util.ArrayList;

public class Zadanie2 {

    public static int[] mergeSort(int[] array, int left, int right) {
        int[] sortedArray = null;
        if (left == right) {
            sortedArray = new int[1];
            sortedArray[0] = array[left];
            return sortedArray;
        }
        int mid = (left + right) / 2;
        int[] subA = mergeSort(array, left, mid);
        int[] subB = mergeSort(array, mid + 1, right);
        sortedArray = merge(subA, subB);
        return sortedArray;
    }
    private static int [] merge(int[] subA, int[] subB) {
        int[] mergedArray = new int[subA.length + subB.length];
        int i, j, k;
        i = 0;
        j = 0;
        k = 0;

        while (i < subA.length && j < subB.length) {
            if (subA[i] < subB[j]) {
                mergedArray[k] = subA[i];
                i++;
            } else if (subA[i] > subB[j]) {
                mergedArray[k] = subB[j];
                j++;
            }
            else {
                mergedArray[k] = subA[i];
                i++;
                j++;
            }
            k++;
        }

        if (j >= subB.length) {
            while (i < subA.length) {
                mergedArray[k] = subA[i];
                i++;
                k++;
            }
        } else {
            while (j < subB.length) {
                mergedArray[k] = subB[j];
                j++;
                k++;
            }
        }
        return mergedArray;
    }
}