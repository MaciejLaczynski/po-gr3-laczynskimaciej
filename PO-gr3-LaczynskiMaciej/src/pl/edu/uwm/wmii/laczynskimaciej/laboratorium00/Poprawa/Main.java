package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Poprawa;
import java.time.LocalDate;
import java.util.ArrayList;
import static pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Poprawa.Stypendium.Kwalifikacja;

public class Main {
    public static void main(String[] args) {
        ArrayList<Student> potential = new ArrayList<>();
        potential.add(new Student("Mariusz Krukowski", LocalDate.of(1995, 5, 3), 3.0, 500));
        potential.add(new Student("Grzegorz Nizzin", LocalDate.of(1995, 5, 3), 3.5, 500));
        potential.add(new Student("Robert Kubica", LocalDate.of(1997, 4, 8), 4.0, 750));
        potential.add(new Student("Stanisław Poniatowski", LocalDate.of(1999, 11, 2), 4.0, 750));
        potential.add(new Student("Jan Gryza", LocalDate.of(2006, 2, 5), 5.0, 1500));
        potential.add(new Student("Tadeusz Gryza", LocalDate.of(2000, 3, 19), 4.5, 1000));

        System.out.print("\nBefore modifications:\n");
        for (Student e : potential) {
            System.out.printf("%s Urodził się %s, posiada stypendium o wartości %d zł i oceną %f .\n",
                    e.getNazwa(), e.getData(), e.getStypendium(), e.getOcena());
        }
        System.out.print("\nStypendium?: \n");
        for (Student e : potential) {
            System.out.println(Kwalifikacja(e));
        }
    }
}