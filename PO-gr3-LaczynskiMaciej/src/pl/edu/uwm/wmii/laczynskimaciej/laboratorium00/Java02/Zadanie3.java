package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java02;

import java.util.Random;

public class Zadanie3 {

    public static void main(String[] Args) {
        neo(3, 3, 2);
    }

    public static void neo(int m, int n, int k) {

        int[][] macierzA = new int[m][n];
        int[][] macierzB = new int[n][k];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                macierzA[i][j] = getRandomNum(-20, 20);
                System.out.format("%4d", macierzA[i][j]);
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < k; j++) {
                macierzB[i][j] = getRandomNum(-20, 20);
                System.out.format("%4d", macierzB[i][j]);
            }
            System.out.println();
        }

    }

    public static int getRandomNum(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }
}