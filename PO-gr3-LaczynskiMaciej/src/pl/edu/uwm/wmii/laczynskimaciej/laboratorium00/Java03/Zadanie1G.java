package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java03;

public class Zadanie1G {

    public static void main(String[] args) {

        System.out.println(nice("2137"));
    }

    public static StringBuilder nice(String str) {
        StringBuilder s = new StringBuilder(str);
        int id = (str.length() - 3);

        while (id > 0)
        {
            s.insert(id, ",");
            id = id - 3;
        }
        return s;
    }
}
