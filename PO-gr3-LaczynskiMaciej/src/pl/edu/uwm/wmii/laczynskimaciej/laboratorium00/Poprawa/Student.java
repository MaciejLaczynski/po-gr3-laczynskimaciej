package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Poprawa;
import java.time.LocalDate;

public class Student implements Cloneable, Comparable<Student> {
    private String nazwa;
    private LocalDate dataUrodziny;
    private Double ocena;
    private Integer doswiadczenie;

    public Student(String name, LocalDate date, Double grade, int experience  ){
        this.nazwa = name;
        this.dataUrodziny = date;
        this.ocena = grade;
        this.doswiadczenie = experience;

    }

    public String getNazwa(){
        return this.nazwa;
    }

    public LocalDate getData(){
        return this.dataUrodziny;
    }

    public Double getOcena(){
        return this.ocena;
    }


    public int getStypendium(){
        return this.doswiadczenie;
    }

    public int compareTo(Student comp) {
        int compare_birth= this.dataUrodziny.compareTo(comp.dataUrodziny);
        if(compare_birth==0){
            return this.dataUrodziny.compareTo(comp.dataUrodziny);
        }
        int compare_nazwa= this.nazwa.compareTo(comp.nazwa);
        if(compare_nazwa==0){
            return this.nazwa.compareTo(comp.nazwa);
        }
        int compare_ocena = this.ocena.compareTo(comp.ocena);
        if(compare_ocena==0){
            return this.ocena.compareTo(comp.ocena);
        }
        return compare_birth;
    }
}