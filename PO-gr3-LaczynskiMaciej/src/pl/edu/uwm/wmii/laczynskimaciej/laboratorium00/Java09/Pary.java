package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java09;

public class Pary {
    public static void main(String[] args)
    {
        String[] words = { "And", "I", "must", "admit", "that", "everything", "I", "witnessed", "has", "been", "cringe" };
        Pair<String> mm = ArrayAlg.minmax(words);
        System.out.println("Przed swap = " + mm.getJeden());
        System.out.println("Przed swap = " + mm.getDwa());
        mm.swap();
        System.out.println("Po swap = " + mm.getJeden());
        System.out.println("Po swap = " + mm.getDwa());
    }
}

class ArrayAlg {
    public static Pair<String> minmax(String[] a)
    {
        if (a == null || a.length == 0) {
            return null;
        }
        String min = a[0];
        String max = a[0];
        for (int i = 1; i < a.length; i++) {
            if (min.compareTo(a[i]) > 0) {
                min = a[i];
            }
            if (max.compareTo(a[i]) < 0) {
                max = a[i];
            }
        }
        return new Pair<>(min, max);
    }
}
