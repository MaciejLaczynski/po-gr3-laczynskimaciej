package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java04;
import java.util.ArrayList;

public class Zadanie2 {

    public static void main(String[] args){
        ArrayList<Integer> arr1 = new ArrayList<>(4);
        arr1.add(1);
        arr1.add(4);
        arr1.add(9);
        arr1.add(16);

        ArrayList<Integer> arr2 = new ArrayList<>(5);
        arr2.add(9);
        arr2.add(7);
        arr2.add(4);
        arr2.add(9);
        arr2.add(11);

        System.out.println(arr1);
        System.out.println(arr2);
        System.out.println(merge(arr1, arr2));
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> arrMerged = new ArrayList<>();
        int ca = 0;
        int cb = 0;
        while(ca < a.size() || cb < b.size()){
            if(ca < a.size())
                arrMerged.add(a.get(ca++));
            if(cb < b.size())
                arrMerged.add(b.get(cb++));
        }
        return arrMerged;
    }
}