package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java05;

public class RachunekBankowy {

    private static double RocznaStopaProcentowa;
    private double Oszczednosci;

    public static void main(String[] args){
        RachunekBankowy test = new RachunekBankowy();
        SetRocznaStopaProcentowa(0.05);
        test.Saldo(3000);
        System.out.println(test.Oszczednosci());
        test.ObliczMiesieczneOdsetki();
        System.out.println(test.Oszczednosci());
    }

    public void Saldo(double balance){
        this.Oszczednosci = balance;
    }
    public void ObliczMiesieczneOdsetki(){
        this.Oszczednosci = this.Oszczednosci + (this.Oszczednosci * RocznaStopaProcentowa) / 12;
    }
    public static void SetRocznaStopaProcentowa(double level){
        RocznaStopaProcentowa = level;
    }
    public double Oszczednosci() {
        return this.Oszczednosci;
    }
}