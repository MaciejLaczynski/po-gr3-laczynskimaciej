package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2_5 {

    public static void main(String[] args){
        int[] ciag = {6,9,4,-8,9,11,-10,-559,11,-4,21,37};
        int pary = 0;
        for(int i = 0; i< ciag.length-1; i++) {
            if (ciag[i] > 0 & ciag[i + 1] > 0) {
                pary++;
                System.out.println("Znalezione pary (" + ciag[i] + "," + ciag[i + 1] + ")");
            }
        }
        System.out.println("Suma wszystkich par: " +pary);
    }
}
