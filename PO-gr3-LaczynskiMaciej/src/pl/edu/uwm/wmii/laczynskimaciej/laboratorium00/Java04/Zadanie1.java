package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java04;
import java.util.ArrayList;

public class Zadanie1 {

    public static void main(String[] args){
        ArrayList<Integer> arr1 = new ArrayList<>(4);
            arr1.add(1);
            arr1.add(4);
            arr1.add(9);
            arr1.add(16);

        ArrayList<Integer> arr2 = new ArrayList<>(5);
            arr2.add(9);
            arr2.add(7);
            arr2.add(4);
            arr2.add(9);
            arr2.add(11);

        System.out.println(arr1);
        System.out.println(arr2);
        System.out.println(append(arr1, arr2));
    }

    public static ArrayList<Integer> append(ArrayList<Integer> arr1, ArrayList<Integer> arr2){
        ArrayList<Integer> arrL = new ArrayList<>();
        arrL.addAll(0, arr1);
        arrL.addAll(arr1.size(), arr2);
        return arrL;
    }
}

