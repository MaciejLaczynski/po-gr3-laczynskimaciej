package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java02;
import java.util.Random;

public class Zadanie2 {
    public static void main(String[] args) {
        int[] tab = new int[70];
        generuj(tab, 69, -999, 999);
        System.out.println(ileNieparzystych(tab));
        System.out.println(ileParzystych(tab));
        System.out.println(Dodatnie(tab));
        System.out.println(Ujemne(tab));
        System.out.println(Zera(tab));
        System.out.println(ileMaksymalnych(tab));
        System.out.println(SumaDodatnich(tab));
        System.out.println(SumaUjemnych(tab));
        System.out.println(dlugoscMaksCiagDod(tab,21));
        Sigma(tab);
    }
    public static int getRandomNum(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }

    public static void generuj(int[] tab, int n, int min, int max){
        System.out.print("[");
        for (int i=0; i<=n; i++){
            tab[i] = getRandomNum(min, max);
            System.out.print(tab[i] + " ");
        }
        System.out.print("]");
        System.out.println();
    }
    public static int ileNieparzystych(int[] tab){
        int Nieparz = 0;
        for(int elem: tab){
            if(!(elem % 2 == 0))
                Nieparz++;
        }
        System.out.println("Mamy " + Nieparz + " liczb nieparzystych");
        return Nieparz;
    }

    public static int ileParzystych(int[] tab) {
        int Parz = 0;
        for (int elem : tab) {
            if (elem % 2 == 0)
                Parz++;
        }
        System.out.println("Mamy " + Parz + " liczb parzystych");
        return Parz;
    }

    public static int Dodatnie(int[] tab) {
        int dodatnie = 0;
        for (int elem : tab) {
            if (elem >= 0)
                dodatnie++;
        }
        System.out.println("Mamy " + dodatnie + " liczb dodatnich");
        return dodatnie;
    }

    public static int Ujemne(int[] tab) {
        int ujemne = 0;
        for (int elem : tab) {
            if (elem < 0)
                ujemne++;
        }
        System.out.println("Mamy " + ujemne + " liczb ujemnych");
        return ujemne;
    }

    public static int Zera(int[] tab) {
        int zera = 0;
        for (int elem : tab) {
            if (elem == 0)
                zera++;
        }
        System.out.println("Mamy " + zera + " zer");
        return zera;
    }

    public static int ileMaksymalnych(int[] tab) {
        int maks = -999;
        for (int elem : tab) {
            if (elem > maks)
                maks = elem;
        }
        System.out.println("Wartość maksymalna to: " + maks + "");
        return maks;
    }

    public static int SumaDodatnich(int[] tab) {
        int SumDod = 0;
        for (int elem : tab) {
            if (elem >= 0)
                SumDod = SumDod + elem;
        }
        System.out.println("Suma liczb dodatnich wynosi " + SumDod + "");
        return SumDod;
    }

    public static int SumaUjemnych(int[] tab) {
        int SumaUje = 0;
        for (int elem : tab) {
            if (elem < 0)
                SumaUje = SumaUje + elem;
        }
        System.out.println("Suma liczb ujemnych wynosi " + SumaUje + "");
        return SumaUje;
    }

    public static int dlugoscMaksCiagDod(int[] tab, int n) {
        int Dmcd = 0, DlugoscObecna = 0;

        for (int k = 0; k < n; k++){
            if(tab[k] > 0){
                DlugoscObecna++;
            }
            else{
                if(DlugoscObecna > Dmcd){
                    Dmcd = DlugoscObecna;
                }
                DlugoscObecna = 0;
            }
        }
        if(Dmcd > 0){
            System.out.println("Długość maksymalnego ciągu dodatnich wynosi " + Dmcd + "");
            return Dmcd;
        }
        else
            return 0;
    }
    public static void Sigma(int[] tab) {
        System.out.print("[");
        for (int i = 0; i < (tab.length); i++) {
            if (tab[i] < 0)
                System.out.print("-1 ");
            else if (tab[i] == 0)
                System.out.print("0 ");
            else if (tab[i] > 0)
                System.out.print("1 ");

        }
        System.out.print("]");
        System.out.println();
    }
}
