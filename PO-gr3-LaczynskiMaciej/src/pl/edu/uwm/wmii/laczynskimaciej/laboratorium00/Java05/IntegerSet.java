package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java05;
import java.util.stream.IntStream;

public class IntegerSet {

    public static void main(String[] args){

        IntegerSet setA = new IntegerSet();
        IntegerSet setB = new IntegerSet();
        setA.insertElement(2);
        setA.insertElement(1);
        setB.insertElement(3);
        setB.insertElement(1);
        System.out.println("A: " + setA);
        System.out.print("B: " + setB);
        System.out.println();
        System.out.println("Złączenie A z B: " + IntegerSet.union(setA, setB));
        System.out.println("Element wspólny A i B: " + IntegerSet.intersection(setA, setB));
        setA.deleteElement(1);
        System.out.println("A po usunięciu elementu 1: " + setA);
    }

    private final boolean[] set;
    public IntegerSet(){
        this.set = new boolean[100];
    }
    public static IntegerSet union(IntegerSet a, IntegerSet b){
        IntegerSet sum_n = new IntegerSet();
        IntStream.range(0, 100).filter(i -> a.set[i] || b.set[i]).forEach(i -> sum_n.set[i] = true);
        return sum_n;
    }
    public static IntegerSet intersection(IntegerSet a, IntegerSet b){
        IntegerSet sum_n = new IntegerSet();
        IntStream.range(0, 100).filter(i -> a.set[i] && b.set[i]).forEachOrdered(i -> sum_n.set[i] = true);
        return sum_n;
    }
    public void insertElement(int i){
        this.set[i-1]=true;
    }
    public void deleteElement(int i){
        this.set[i-1]=false;
    }

    public String toString(){
        StringBuilder number= new StringBuilder();
        for(int i = 0; i < 100; i++){
            if(this.set[i]){
                number.append(i+1);
                number.append(" ");
            }
        }
        return number.toString();
    }
    public boolean equals(IntegerSet b){
        return this.toString().equals(b.toString());
    }
}