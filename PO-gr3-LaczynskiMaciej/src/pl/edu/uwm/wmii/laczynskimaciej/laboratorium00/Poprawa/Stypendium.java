package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Poprawa;
import java.time.LocalDate;
import java.util.ArrayList;

public class Stypendium {
    private static int kwota;
    private ArrayList<Student> k;

    public Stypendium(int exp, ArrayList<Student> k){
        Stypendium.kwota = exp;
        this.k = k;
    }

    public static void setStypendium(){
        kwota = kwota + 500;
    }
    public static int getStyp(){
        return kwota;
    }

    static LocalDate cutoff = LocalDate.parse("2005-01-01");

    public static int Kwalifikacja(Student k) {
        if(k.getOcena() == 5 && k.getData().isAfter(cutoff)) {
            setStypendium();
            return getStyp();
        }
        return getStyp();
    }
}

