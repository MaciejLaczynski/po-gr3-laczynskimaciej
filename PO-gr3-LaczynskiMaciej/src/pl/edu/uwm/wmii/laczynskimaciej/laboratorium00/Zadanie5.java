package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00;

public class Zadanie5 {
    public static void main(String[] args) {
        box("Java");
    }

    public static void box(String contents){
        int n = contents.length();
        System.out.print("+");
        for (int i = 2; i < n + 2; i++){
            System.out.print("-");
        }
        System.out.print("+");
        System.out.println();
        System.out.println("|" + contents + "|");
        System.out.print("+");
        for (int i = 2; i < n + 2; i++){
            System.out.print("-");
        }
        System.out.print("+");
        System.out.println();
    }
}
