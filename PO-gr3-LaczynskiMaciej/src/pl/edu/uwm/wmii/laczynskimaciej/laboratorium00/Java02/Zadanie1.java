package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java02;

import java.util.Random;

public class Zadanie1 {
    public static void main(String[] args){
        int ilosc = 20;
        int max = 99;
        int min = -99;
        int[] array = new int[ilosc];


        System.out.print("[ ");
        for (int i=0; i<ilosc; i++){
            Random random = new Random();
            int int_random = random.nextInt(max-min)+min;
            array[i] = int_random;
            System.out.print(array[i] + " ");
        }
        System.out.print("]");
        System.out.println();


        int Nieparz = 0;
        int Parz = 0;
        for(int elem: array){
            if(!(elem % 2 ==0))
                Nieparz++;
            else
                Parz++;
        }
        System.out.println("Ilość wartości parzystych: " + Parz + "  Ilość wartości nieparzystych: " + Nieparz);


        int ujemne = 0;
        int dodatnie = 0;
        int zera = 0;
        for (int elem : array) {
            if (elem < 0)
                ujemne++;
            else if (elem > 0)
                dodatnie++;
            else
                zera++;
        }
        System.out.println("Ilości wartości dodatnich: " + dodatnie + ", wartości ujemnych: " + ujemne + ", zer: " + zera + ".");


        int najwiekszy = -999;
        int wyst = 0;
        for (int elem : array) {
            if (elem > najwiekszy)
                najwiekszy = elem;
        }
        for(int elem : array){
            if(elem == najwiekszy)
                wyst++;
        }
        System.out.println("Największy element:  " + najwiekszy + " występuje: " + wyst + " razy ");

        int Suma_Poz = 0;
        int Suma_Neg = 0;
        for (int elem : array) {
            if (elem >= 0)
                Suma_Poz = Suma_Poz + elem;
            else
                Suma_Neg = Suma_Neg + elem;
        }
        System.out.println("Suma wartości pozytywnych wynosi: " + Suma_Poz + "  Suma wartości negatywnych wynosi: " + Suma_Neg);


        int dlugosc_max = 0, dlugosc_ob = 0;
        for (int i = 0; i < ilosc; i++){
            if(array[i] > 0){
                dlugosc_ob++;
            }
            else{
                if(dlugosc_ob > dlugosc_max){
                    dlugosc_max = dlugosc_ob;
                }
                dlugosc_ob = 0;
            }
        }
        if(dlugosc_max > 0)
            System.out.println("Długość ciągu wartości dodatnich wynosi " + dlugosc_max);
        else
            System.out.println("Nie ma wartości dodatnich które formują ciąg");


        System.out.print("[ ");
        for (int i = 0; i < ilosc; i++) {
            if (array[i] < 0)
                System.out.print("-1 ");
            else if (array[i] == 0)
                System.out.print("0 ");
            else if (array[i] > 0)
                System.out.print("1 ");
        }
        System.out.print("]");
        System.out.println();
    }
}

