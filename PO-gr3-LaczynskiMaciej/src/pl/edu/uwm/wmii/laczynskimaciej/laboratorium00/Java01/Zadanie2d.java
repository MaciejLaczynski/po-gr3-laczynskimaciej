package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2d {

    public static void main (String[] args){

        int[] ciag = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,69,420,911,2137,9999};

        int zsum = 0;
        for( double v : ciag){
            if(v < ((v - 1 + v + 1)/2) & v > 1 & v < ciag.length) {
                zsum = zsum + 1;
                System.out.println("liczba " + v + " spełnia wszystkie warunki");
            }
        }
        System.out.println("Mamy " + zsum + " liczb spełniających podane warunki");
    }
}
// Nie mogłem znaleźć liczby spełniającej ten warunek