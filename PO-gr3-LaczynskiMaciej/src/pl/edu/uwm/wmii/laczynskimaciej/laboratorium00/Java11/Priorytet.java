package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java11;
import java.util.*;

public class Priorytet {
    public Priorytet(){
        this.lista = new PriorityQueue<>();
        this.disp();
    }

    public void disp(){
        Scanner keyboard = new Scanner(System.in);
        String[] words;
        String line = keyboard.nextLine();
        Iterator<String> it;
        while (!line.equals("zakoncz")){
            words = line.split(" ");
            it = Arrays.stream(words).iterator();
            if(it.hasNext()){
                it.next();
                if(words[0].equals("dodaj")){
                    System.out.println("Added entry.");
                    int prio = 0;
                    StringBuilder writer= new StringBuilder();
                    if(it.hasNext()){
                        prio=Integer.parseInt(it.next());
                    }
                    while (it.hasNext()){
                        writer.append(it.next());
                        writer.append(" ");
                    }
                    lista.add(new Zadanie(prio,writer.toString()));
                }
                else if(words[0].equals("dalej")){
                    lista.remove();
                    System.out.println("Rekord usunięty.");
                }
                else {
                    System.out.println("Nierozpoznano komendy.");
                }
            }
            line = keyboard.nextLine();

        }
    }

    public void wypisz(){
        PriorityQueue<Zadanie> copier = new PriorityQueue<>(this.lista);
        while(!copier.isEmpty()){
            Zadanie e = copier.remove();
            System.out.println("Priorytet: " + e.priorytet);
            System.out.println("Opis: " + e.opis);
        }
    }

    private PriorityQueue <Zadanie> lista;
}

class Zadanie implements Comparable<Zadanie>{
    public Zadanie(int priorytet, String opis){
        this.opis = opis;
        this.priorytet = priorytet;
    }

    int priorytet;
    String opis;

    @Override
    public int compareTo(Zadanie o) {
        return Integer.compare(this.priorytet, o.priorytet);
    }
}
