package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java09;

public class Pair<T> {
    private T jeden;
    private T dwa;

    public Pair() {
        jeden = null;
        dwa = null;
    }

    public Pair (T jeden, T second) {
        this.jeden = jeden;
        this.dwa = second;
    }

    public T getJeden() {
        return jeden;
    }
    public T getDwa() {
        return dwa;
    }

    public void setJeden(T newValue) {
        jeden = newValue;
    }
    public void setDwa(T newValue) {
        dwa = newValue;
    }

    public void swap(){
        T holder = this.jeden;
        this.jeden = this.dwa;
        this.dwa = holder;
    }

}

