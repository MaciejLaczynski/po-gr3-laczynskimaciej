package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Test2;
import java.util.ArrayList;
import java.util.HashMap;


class Kandydat implements Cloneable, Comparable<Kandydat> {
    private String nazwa;
    private int wiek;
    private String wyksztalcenie;
    private int doswiadczenie;

    public Kandydat(String nazwa, int wiek, String wyksztalcenie, int doswiadczenie) {
        this.nazwa = nazwa;
        this.wiek = wiek;
        this.wyksztalcenie = wyksztalcenie;
        this.doswiadczenie = doswiadczenie;
    }

    @Override
    public int compareTo(Kandydat other) {

        if (this.wyksztalcenie.compareTo(other.wyksztalcenie) != 0) {
            return this.wyksztalcenie.compareTo(other.wyksztalcenie);
        }

        if (this.wiek != other.wiek) {

            if (this.wiek < other.wiek)
                return -1;
            else
                return 1;

        }

        if (this.doswiadczenie < other.doswiadczenie)
            return -1;

        if (this.doswiadczenie > other.doswiadczenie)
            return 1;

        return 0;
    }

    public String getNazwa() {

        return nazwa;
    }

    public int getDoswiadczenie() {

        return doswiadczenie;
    }

    public int getWiek() {

        return wiek;
    }

    public String getWyksztalcenie() {

        return wyksztalcenie;
    }

    public static boolean Qualified(Kandydat k) {
        if(k.getDoswiadczenie() >= Rekrutacja.doswiadczenie) {
            return true;
        }
        return false;
    }

    public static HashMap<Integer, String> RecruitmentMap(ArrayList<Kandydat> klist) {
        HashMap<Integer, String>  map = new HashMap<>();
        for(Kandydat e : klist)
            if(Qualified(e))
                map.put(e.getDoswiadczenie(), e.getNazwa());
        return map;
    }

}