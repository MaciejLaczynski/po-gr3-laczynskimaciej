package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2_2 {
    public static void main(String[] args) {
        int[] ciag = {1,-2,-3,4,-5,6,7,-8,-9,10};
        int suma = 0;

        for (int j : ciag) {
            if (j > 0) {
                suma = suma + j;
            }
        }
        System.out.println("Suma przed podwojeniem: " + suma);
        System.out.println("Suma po podwojeniu: " + (suma*2));
    }
}
