package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2g {
    public static void main(String[] args) {

        int[] ciag = new int[]{1,-2,3,-4,5,-7,-8,9,10,-20,69,-911,2137};

        int zsum = 0;
        for(int i =0; i< ciag.length; i++) {
            if(ciag[i] % 2 != 0 & ciag[i] > 0) {
                zsum++;
                System.out.println("Liczba " + ciag[i] + " spełnia warunki");
            }
        }
        System.out.println("Mamy " + zsum + " liczb spełniających podane warunki");
    }
}