package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java05;

import java.util.GregorianCalendar;
import java.time.LocalDate;

class pracownik implements Cloneable {

    public static void main(String[] args){

        Pracownik[] pracownicy = new Pracownik[3];
        pracownicy[0] = new Pracownik("Freeman", 150000, 1987, 5, 12);
        pracownicy[1] = new Pracownik("Kleiner", 55000, 1969, 11, 30);
        pracownicy[2] = new Pracownik("Calhoun", 25000, 1989, 6, 2);

        for(Pracownik e : pracownicy)
            e.zwiekszPobory(25);

        for(Pracownik f : pracownicy){
            System.out.print("Nazwisko: " + f.nazwisko() + "\tWypłata: " + f.wyplata());
            System.out.print("\tData zatrudnienia: " + f.DataZatrudnienia() + "\n");
        }
    }
}

public class Pracownik implements Cloneable {

    public Pracownik(String nazwisko, double pobory, int rok, int miesiac, int dzien) {
        this.nazwisko = nazwisko;
        this.pobory = pobory;

        GregorianCalendar calendar = new GregorianCalendar(rok, miesiac, dzien);

        this.DataZatrudnienia = LocalDate.of(rok,miesiac,dzien);
    }

    public String nazwisko() {
        return this.nazwisko;
    }

    public double wyplata() {
        return this.pobory;
    }

    public LocalDate DataZatrudnienia() {
        return this.DataZatrudnienia;
    }

    public void zwiekszPobory(double percent) {
        double raise = this.pobory * percent / 100;
        this.pobory += raise;
    }

    private String nazwisko;
    private double pobory;
    private LocalDate DataZatrudnienia;
}
