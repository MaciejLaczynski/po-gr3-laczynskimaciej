package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2c {

    public static void main (String[] args){

        int[] ciag = {1,2,4,8,16,32,64,128};
        int zsum = 0;
        for (int v : ciag) {
            if (v % 4 == 0 & Math.pow(v, 2) % 2 == 0) {
                zsum += 1;
                System.out.println("Liczba " + v + " jest kwadratem liczby parzystej");
            }
        }

        System.out.println("Mamy " + zsum + " liczb będących kwadratami liczb parzystych");
    }
}
