package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java03;

public class Zadanie1C {

    public static void main(String[] args){
        System.out.println(middle("KAJAK"));
    }

    public static String middle(String str){

        String s_mid = "";
        if(str.length() <= 1)
            return "Fraza jest za krótka by miała środek";

        int ml = (int)str.length()/2;
        if(str.length()%2 == 0) {
            s_mid = str.substring(ml - 1, ml + 1);

            System.out.println("Podana fraza nie posiada właściwego środka bo liczba elementów we frazie jest parzysta ");
            return "Środek frazy znajduje się mniej więcej tu: " + s_mid;
        }
        else
            s_mid = str.substring(ml, ml+1);
        return "Środek twojej frazy występuje tutaj: " + s_mid;
    }
}
