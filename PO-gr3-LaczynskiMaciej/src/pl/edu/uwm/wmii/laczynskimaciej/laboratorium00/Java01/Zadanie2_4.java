package pl.edu.uwm.wmii.laczynskimaciej.laboratorium00.Java01;

public class Zadanie2_4 {
    public static void main(String[] args){

        int[] ciag = {2,1,3,7,9,11,4,20,6,9,69,};
        int najw = 0;
        int najmn = 2137;

        for(int i = ciag.length - 1; i >= 0; i--){
            if(ciag[i] < najmn)
                najmn = ciag[i];
            if(ciag[i] > najw)
                najw = ciag[i];
        }
        System.out.println("Najmniejsza liczba w ciągu: " + najmn);
        System.out.println("Największa liczba w ciągu: " + najw);
    }

}
