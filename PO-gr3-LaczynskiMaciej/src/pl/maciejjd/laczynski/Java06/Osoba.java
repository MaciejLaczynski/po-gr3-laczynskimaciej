package pl.maciejjd.laczynski.Java06;

public class Osoba {
    private String Nazwisko;
    private int RokUrodzenia;

    public Osoba(String Nazwisko, int rokUrodzenia){
        this.Nazwisko = Nazwisko;
        this.RokUrodzenia = rokUrodzenia;
    }

    public int getRokUrodzenia(){
        return this.RokUrodzenia;
    }

    public String getNazwisko(){
        return this.Nazwisko;
    }

    public String toString(){
        return "Nazwisko: " + this.Nazwisko + "\nRok Urodzenia: " + this.RokUrodzenia;
    }
}
