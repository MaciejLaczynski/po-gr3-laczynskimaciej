package pl.maciejjd.laczynski.Java06;

public class Nauczyciel extends Osoba{

    private int Pensja;

    public Nauczyciel(String Nazwisko, int RokUrodzenia, int Pensja){
        super(Nazwisko, RokUrodzenia);
        this.Pensja = Pensja;
    }

    public int getPensja(){
        return this.Pensja;
    }

    public String toString(){
        return super.toString() + "\n Pensja: " + this.Pensja;
    }
}