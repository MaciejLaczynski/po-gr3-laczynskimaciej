package pl.maciejjd.laczynski.Java06;

public class Student extends Osoba{

    private String kierunek;

    public Student(String Nazwisko, int RokUrodzenia, String kierunek){
        super(Nazwisko, RokUrodzenia);
        this.kierunek = kierunek;
    }

    public String toString() {
        return super.toString()+"\nKierunek: "+this.kierunek;
    }
}