package pl.maciejjd.laczynski.Java06;

public class Uruchom {

    public static void main(String[] args){

        Adres test1 = new Adres("Gorbatowa", "1",
                "26", "07-410", "Ostrołęka");
        Adres test2 = new Adres("Jana Pawła II", "21",
                "37", "07-410", "Ostrołęka");

        test1.wypisz();
        System.out.println(" ");
        test2.wypisz();
        System.out.println(" ");

        Osoba Pawel = new Osoba("Kowalski", 1993);
        Student Krzysztof = new Student("Zapała", 1987, "Bezpieczeństwo wewnętrzne");
        Nauczyciel Karol = new Nauczyciel("Krzywonosy", 1969, 3400);

        System.out.println(Pawel);
        System.out.println(" ");
        System.out.println(Krzysztof);
        System.out.println(" ");
        System.out.println(Karol);
        System.out.println(Karol.getPensja());

    }
}
