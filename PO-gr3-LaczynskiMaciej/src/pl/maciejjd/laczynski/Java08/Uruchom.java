package pl.maciejjd.laczynski.Java08;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Scanner;

public class Uruchom {
    public static void main(String[] args){

        ArrayList<Osoba> grupa = new ArrayList<>(5);
        grupa.add(new Osoba("Bartosz", LocalDate.of(1997, 12, 24)));
        grupa.add(new Osoba("Oskar", LocalDate.of(1996, 6, 30)));
        grupa.add(new Osoba("Maciej", LocalDate.of(1999, 5, 18)));
        grupa.add(new Osoba("Oskar", LocalDate.of(2000, 8, 25)));
        grupa.add(new Osoba("Dariusz", LocalDate.of(1999, 5, 18)));
        System.out.println(grupa.get(0));
        System.out.println(grupa.get(0).equals(grupa.get(1)));

        System.out.println(grupa);
        Collections.sort(grupa);
        System.out.println(grupa);

        ArrayList<Osoba> students = new ArrayList<>(3);
        students.add(new Student("Bartosz", LocalDate.of(1997, 12, 24), 4.33));
        students.add(new Student("Jakub", LocalDate.of(1998, 5, 24), 5.0));
        students.add(new Student("Adrian", LocalDate.of(1999, 12, 31), 2.42));

        System.out.println(students);
        Collections.sort(students);
        System.out.println(students);

        if(args.length != 0){
            ArrayList<String> a3 = new ArrayList<>();
            try{
                File tester = new File(args[0]);
                Scanner reader = new Scanner(tester);
                while(reader.hasNextLine()){
                    a3.add(reader.nextLine());
                }
                reader.close();
            }
            catch(FileNotFoundException a){
                System.out.println("Plik nie istnieje lub nie może zostać znaleziony");
                a.printStackTrace();
            }
            System.out.println(a3);
            Collections.sort(a3);
            System.out.println(a3);
        }
    }
}