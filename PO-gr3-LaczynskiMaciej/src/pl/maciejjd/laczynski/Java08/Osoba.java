package pl.maciejjd.laczynski.Java08;
import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba> {

    private String imie;
    private LocalDate dataUrodzenia;

    public Osoba(String Imie, LocalDate DataUrodzenia){
        this.imie = Imie;
        this.dataUrodzenia = DataUrodzenia;
    }

    public String getImie(){
        return this.imie;
    }

    public LocalDate getDataUrodzenia(){
        return this.dataUrodzenia;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"( "+this.imie +", urodzony "+this.dataUrodzenia.toString()+" )";
    }

    @Override
    public boolean equals(Object obj) {
        Osoba osb = (Osoba) obj;
        return (osb.imie.equals(this.imie) && osb.dataUrodzenia.equals(this.dataUrodzenia));
    }

    @Override
    public int compareTo(Osoba comp) {
        int compare_imie= this.imie.compareTo(comp.imie);
        if(compare_imie==0){
            return this.dataUrodzenia.compareTo(comp.dataUrodzenia);
        }
        return compare_imie;
    }
}
