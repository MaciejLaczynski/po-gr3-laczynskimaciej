package pl.maciejjd.laczynski.Java08;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable<Osoba>{

    private double sredniaOcen;

    public Student(String Imie, LocalDate DataUrodzenia, double srednia){
        super(Imie, DataUrodzenia);
        this.sredniaOcen = srednia;
    }

    @Override
    public String toString(){
        return this.getClass().getSimpleName() + "( " + this.getImie() + " ,urodzony " + this.getDataUrodzenia().toString()
                + " ,średnia ocen: " + this.sredniaOcen + " )";
    }

    @Override
    public int compareTo(Osoba comp){
        int last = super.compareTo((comp));
        if((comp instanceof Student) && (last == 0)){
            return -(int)Math.ceil(this.sredniaOcen - ((Student) comp).sredniaOcen);
        }
        return last;
    }
}
