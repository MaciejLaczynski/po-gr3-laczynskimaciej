package pl.maciejjd.laczynski.Java07;

import java.time.LocalDate;

public class Uruchom {

    public static void main(String[] args){

        Osoba[] people = new Osoba[3];

        people[0] = new Pracownik("Dżehanowsky", new String[]{"Dominik"}, LocalDate.of(1999, 11, 6),
                false, 21370, LocalDate.of(2016, 8, 3));

        people[1] = new Student("Stashkiewich", new String[]{"Krystian"}, LocalDate.of(1999, 7, 23),
                false, "Informatyka", 3.51);

        people[2] = new Student("Poolawska", new String[]{"Aleksandra"}, LocalDate.of(2000, 12, 18),
                true, "Medycyna", 4.20);

        for(Osoba p : people){
            for(String i: p.getImie()){
                System.out.print(i + " ");
            }
            System.out.print(p.getNazwisko() + " - " + p.getEntry()
                    + " \nUrodził/ła się " + p.getDataUrodzenia() + " płeć tej osoby to ");
            if(p.getPlec()){
                System.out.println("kobieta.\n");
            }
            else
            {
                System.out.println("mężczyzna.\n");
            }
        }

    }
}