package pl.maciejjd.laczynski.Java07;
import java.time.LocalDate;

public abstract class Osoba {

    private String nazwisko;
    private String[] imie;
    private LocalDate dataUrodzenia;
    private boolean plec;

    public Osoba(String Nazwisko, String[] Imie, LocalDate dataUrodzenia, boolean Plec){
        this.nazwisko = Nazwisko;
        this.imie = Imie;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = Plec;
    }

    public abstract String getEntry();

    public String getNazwisko(){
        return nazwisko;
    }

    public String[] getImie(){
        return imie;
    }

    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }

    public boolean getPlec(){
        return plec;
    }
}