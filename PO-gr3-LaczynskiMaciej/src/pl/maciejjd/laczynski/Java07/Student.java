package pl.maciejjd.laczynski.Java07;
import java.time.LocalDate;

public class Student extends Osoba{

    public String program;
    public double avgOcena;

    public Student(String Nazwisko, String[] Imie, LocalDate dataUrodzenia, boolean Plec, String Kierunek, double sredniaOcen){
        super(Nazwisko, Imie, dataUrodzenia, Plec);
        this.program = Kierunek;
        this.avgOcena = sredniaOcen;
    }

    public double getAvgOcena(){
        return avgOcena;
    }

    public void setAvgOcena(double entered){
        this.avgOcena = entered;
    }

    @Override
    public String getEntry(){
        return "Ta osoba jest na kierunku " + this.program + " ze średnią ocen " + this.avgOcena;
    }
}