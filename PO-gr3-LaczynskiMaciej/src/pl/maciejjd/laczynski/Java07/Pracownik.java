package pl.maciejjd.laczynski.Java07;
import java.time.LocalDate;

public class Pracownik extends Osoba {

    private double wyplata;
    private LocalDate datazatrudnienia;

    public Pracownik(String Nazwisko, String[] Imie, LocalDate dataUrodzenia, boolean Plec, double Wyplata, LocalDate dataZatrudnienia){
        super(Nazwisko, Imie, dataUrodzenia, Plec);
        this.wyplata = Wyplata;
        this.datazatrudnienia = dataZatrudnienia;
    }

    public double getWyplata(){
        return wyplata;
    }

    public LocalDate getDataZatrudnienia(){
        return datazatrudnienia;
    }

    @Override
    public String getEntry() {
        return String.format("Tego pracownika zatrudniono %s, z zarobkiem %.2f zł.",
                this.datazatrudnienia.toString(), this.wyplata);
    }
}