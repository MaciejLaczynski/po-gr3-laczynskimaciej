package pl.maciejjd.laczynski.Java07.Instrumenty;

import java.time.LocalDate;

public class Skrzypce extends Instrument{
    public Skrzypce(String Producent, LocalDate dataProdukcji){
        super(Producent, dataProdukcji);
    }

    public String sound(){
        return "*8 years old starts castrating your ears with this very instrument*";
    }
}
