package pl.maciejjd.laczynski.Java07.Instrumenty;
import java.time.LocalDate;

public class Flet extends Instrument{
    public Flet(String Producent, LocalDate dataProdukcji){
        super(Producent, dataProdukcji);
    }

    public String sound(){
        return "*Rules of Nature flute cover starts playing in the distance*";
    }
}
