package pl.maciejjd.laczynski.Java07.Instrumenty;
import java.time.LocalDate;

public class Fortepian extends Instrument{
    public Fortepian(String Producent, LocalDate dataProdukcji){
        super(Producent, dataProdukcji);
    }

    public String sound(){
        return "*Tom starts playing Hungarian Rhapsody No.2*";
    }
}