package pl.maciejjd.laczynski.Java07.Instrumenty;
import java.time.LocalDate;


public class Uruchom {

    public static void main(String[] args) {
        Instrument[] specifics = new Instrument[3];

        specifics[0] = new Flet("Jupiter", LocalDate.of(2001, 3, 2));
        specifics[1] = new Skrzypce("Stentor", LocalDate.of(2012, 6, 12));
        specifics[2] = new Fortepian("Steinway & Sons", LocalDate.of(2008, 8, 26));

        for (Instrument ins : specifics) {
            System.out.print(ins.sound() + "\n");
            System.out.println(ins);
        }
    }
}
