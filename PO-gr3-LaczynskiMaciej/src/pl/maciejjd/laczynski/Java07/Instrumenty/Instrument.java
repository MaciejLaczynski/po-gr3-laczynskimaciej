package pl.maciejjd.laczynski.Java07.Instrumenty;
import java.time.LocalDate;

public abstract class Instrument {

    private String Producent;
    private LocalDate dataProdukcji;
    private Object obj;

    public Instrument(String manufacturer, LocalDate dOp){
        this.Producent = manufacturer;
        this.dataProdukcji = dOp;
    }

    public abstract String sound();

    public String getProducent() {
        return this.Producent;
    }

    public LocalDate getDataProdukcji(){
        return this.dataProdukcji;
    }

    @Override
    public boolean equals(Object obj) {
        return this.toString().equals(obj.toString());
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+" został stworzony przez " + this.Producent
                + " w "+this.dataProdukcji+"\n";
    }
}